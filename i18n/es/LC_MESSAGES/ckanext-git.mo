��          �               <     =     D     X  "   _     �     �     �     �  %   �     �     �     �     �  +        ?     E  ,   ]  �  �               -  *   3     ^  	   k     u     |  )   �     �  $   �     �     �  2        >     F  8   e   Accept Check modifications Create Create a new modification proposal Description Discard Edit Pending modifications Pending modifications (Administrator) Resource Resource view not found Status Submit modification There are no pending modifications to show. Title eg. A descriptive title eg. Some useful notes about the modification Project-Id-Version: ckanext-git 0.0.1
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2016-02-10 15:09+0100
PO-Revision-Date: 2016-02-10 14:34+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.6
 Acceptar Revisar modificaciones Crear Crear una nueva propuesta de modificación Descripción Descartar Editar Modificaciones pendientes Modificaciones pendientes (Administrador) Recurso No se encuentra la vista del recurso Estado Publicar modificación No existen modificaciones pendientes para mostrar. Título p. ej.: un título descriptivo p. ej.: algunas notas útiles acerca de la modificación 