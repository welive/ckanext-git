"""Tests for plugin.py."""
from ckanext.git.model import GitBranch
import ckan.tests.legacy as tests
import ckan.model as model
import ckan.tests.factories as factories
import ckan
import pylons.config as config
import webtest
import mock
import json
import os
import ConfigParser
import unittest

RESOURCE_URL = 'http://foo/bar.txt'

ORIGINAL_TEXT = 'This is the original text\n'
MODIFIED_TEXT = 'This is the modified text\n'
MOD_TITLE = 'Test modification'
MOD_DESCRIPTION = 'This text has been modified'

ckan_config = ConfigParser.ConfigParser()
ckan_config.read(os.environ['CKAN_CONFIG'])

PLUGIN_SECTION = 'plugin:git'
REPO_DIR = ckan_config.get(PLUGIN_SECTION, 'repo_dir')


def create_repo(app):
    admin = factories.Sysadmin()
    dataset = factories.Dataset()
    resource = factories.Resource(package_id=dataset['id'],
                                  url=RESOURCE_URL)

    app.post('/dataset/{}/resource/{}/git/new'.format(dataset['id'],
                                                      resource['id']),
             {'title': MOD_TITLE,
              'notes': MOD_DESCRIPTION,
              'modifications': MODIFIED_TEXT},
             headers={'Authorization': str(admin['apikey'])})

    return dataset, resource

class MockResponse(object):
    def __init__(self, content, url):
        self.content = content
        self.url = url
        self.text = content

    def json(self):
        return json.loads(self.content)


class TestGitUser(unittest.TestCase):

    def setUp(self):
        model.repo.init_db()
        app = ckan.config.middleware.make_app(config['global_conf'], **config)
        self.app = webtest.TestApp(app)

        ckan.plugins.load('git')

    def tearDown(self):
        model.repo.rebuild_db()
        ckan.plugins.unload('git')

    def _get_app(self):
        return self.app

    @mock.patch('requests.get')
    def test_create_modification(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)

        app = self._get_app()

        dataset, resource = create_repo(app)

        branch = GitBranch.get()

        assert(branch.title == MOD_TITLE)
        assert(branch.description == MOD_DESCRIPTION)
        assert(branch.resource_id == resource['id'])
        assert(branch.status == 'pending')

        assert(os.path.isfile('{}/{}/bar.txt'.format(REPO_DIR,
                                                     resource['id'])))

        f = open('{}/{}/bar.txt'.format(REPO_DIR, resource['id']), 'r')
        assert(f.read() == ORIGINAL_TEXT)

    @mock.patch('requests.get')
    def test_edit_modification(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)

        app = self._get_app()
        admin = factories.Sysadmin()
        dataset, resource = create_repo(app)

        branch = GitBranch.get()
        app.post('/dataset/{}/resource/{}/git/edit/{}'.format(
            dataset['id'], resource['id'], branch.id),
                 {'title': MOD_TITLE,
                  'notes': MOD_DESCRIPTION,
                  'modifications': ORIGINAL_TEXT},
                 headers={'Authorization': str(admin['apikey'])})

        assert(os.path.isfile('{}/{}/bar.txt'.format(REPO_DIR,
                                                     resource['id'])))
        f = open('{}/{}/bar.txt'.format(REPO_DIR, resource['id']), 'r')
        assert(f.read() == ORIGINAL_TEXT)

    @mock.patch('requests.get')
    def test_show_modification(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)

        app = self._get_app()
        dataset, resource = create_repo(app)

        branch = GitBranch.get()

        app.get('/dataset/{}/resource/{}/git/edit/{}'.format(
            dataset['id'], resource['id'], branch.id))

    @mock.patch('requests.get')
    def test_show_modification_form(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)

        app = self._get_app()
        dataset, resource = create_repo(app)

        app.get('/dataset/{}/resource/{}/git/new/'.format(
            dataset['id'], resource['id']))

    @mock.patch('requests.get')
    def test_list_branches(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)
        admin = factories.Sysadmin()
        app = self._get_app()
        dataset, resource = create_repo(app)

        app.get('/dataset/{}/resource/{}/git/branches'.format(
            dataset['id'], resource['id']),
            headers={'Authorization': str(admin['apikey'])})

    @mock.patch('requests.get')
    def test_list_branches_errors(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)
        admin = factories.Sysadmin()
        app = self._get_app()
        dataset, resource = create_repo(app)

        app.get('/dataset/{}/resource/{}/git/branches?error=true'.format(
            dataset['id'], resource['id']),
            headers={'Authorization': str(admin['apikey'])})

    @mock.patch('requests.get')
    def test_check_branches(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)
        app = self._get_app()
        dataset, resource = create_repo(app)

        app.get('/dataset/{}/resource/{}/git/list'.format(
            dataset['id'], resource['id']))

    @mock.patch('requests.get')
    def test_check_branch(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)
        app = self._get_app()
        dataset, resource = create_repo(app)
        branch = GitBranch.get()

        app.get('/dataset/{}/resource/{}/git/check/{}'.format(
            dataset['id'], resource['id'], branch.id))

    @mock.patch('shutil.copyfile')
    @mock.patch('ckanext.git.utils.sync_datapusher')
    @mock.patch('requests.get')
    def test_accept_modification(self, mock_requests_get,
                                 mock_sync_datapusher, mock_copyfile):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)

        app = self._get_app()

        dataset, resource = create_repo(app)

        branch = GitBranch.get()

        assert(branch.status == 'pending')

        app.post('/dataset/{}/resource/{}/git/check/{}/accept'.format(
            dataset['id'], resource['id'], branch.id))

        branch = GitBranch.get()
        assert(branch.status == 'accepted')

        f = open('{}/{}/bar.txt'.format(REPO_DIR, resource['id']), 'r')
        assert(f.read() == MODIFIED_TEXT)

    @mock.patch('requests.get')
    def test_discard_modification(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)

        app = self._get_app()

        dataset, resource = create_repo(app)

        branch = GitBranch.get()
        assert(branch.status == 'pending')

        app.post('/dataset/{}/resource/{}/git/check/{}/discard'.format(
            dataset['id'], resource['id'], branch.id))

        branch = GitBranch.get()
        assert(branch.status == 'discarded')

        f = open('{}/{}/bar.txt'.format(REPO_DIR, resource['id']), 'r')
        assert(f.read() == ORIGINAL_TEXT)

    @mock.patch('requests.get')
    def test_delete_branch(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)
        app = self._get_app()
        dataset, resource = create_repo(app)
        branch = GitBranch.get()

        app.get('/dataset/{}/resource/{}/git/{}/delete'.format(
            dataset['id'], resource['id'], branch.id))

        branch_result = GitBranch.get()
        assert(branch_result is None)

    @mock.patch('requests.get')
    def test_delete_dataset(self, mock_requests_get):
        mock_requests_get.return_value = MockResponse(ORIGINAL_TEXT,
                                                      RESOURCE_URL)
        admin = factories.Sysadmin()
        app = self._get_app()
        dataset, resource = create_repo(app)

        tests.call_action_api(self.app, 'resource_delete', id=resource['id'],
                              apikey=admin['apikey'])
