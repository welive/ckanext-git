import git
import requests
import os
import ConfigParser
import shutil
from ckan.logic.action import get
import ckan
import logging
import json
import redis
import pylons

from pylons.controllers.util import redirect

from requests.exceptions import ConnectionError

log = logging.getLogger(__name__)

config = ConfigParser.ConfigParser()
config.read(os.environ['CKAN_CONFIG'])

MAIN_SECTION = 'app:main'
DATAPUSHER_URL = config.get(MAIN_SECTION, 'ckan.datapusher.url')
SITE_URL = config.get(MAIN_SECTION, 'ckan.site_url')

REDIS_SECTION = 'redis'
REDIS_HOST = config.get(REDIS_SECTION, 'redis.host')
REDIS_PORT = config.get(REDIS_SECTION, 'redis.port')
REDIS_DB = config.get(REDIS_SECTION, 'redis.db')

PLUGIN_SECTION = 'plugin:git'
REPO_DIR = config.get(PLUGIN_SECTION, 'repo_dir')

AAC_SECTION = 'plugin:authentication'
AAC_URL = config.get(AAC_SECTION, 'aac_url')
CLIENT_ID = config.get(AAC_SECTION, 'client_id')
CLIENT_SECRET = config.get(AAC_SECTION, 'client_secret')


def sync_datapusher(resource_id):
    token = pylons.session['ckanext-welive-token']
    url = DATAPUSHER_URL + 'job'
    headers = {'Content-Type': 'application/json'}
    payload = {'job_type': 'push_to_datastore',
               'api_key': token,
               'metadata': {'resource_id': resource_id,
                            'ckan_url': SITE_URL
                            }
               }

    response = requests.post(url, data=json.dumps(payload), headers=headers)
    log.debug(response.content)


def create_repo(resource):
    repo = git.Repo.init(os.path.join(REPO_DIR, resource['id']))
    try:
        response = requests.get(resource['url'])
        resource_file_name = response.url.rsplit('/', 1)[-1]
        resource_path = os.path.join(REPO_DIR, resource['id'],
                                     resource_file_name)
        f = open(resource_path, 'w')
        f.write(response.text.encode('utf-8'))
        f.close()
        repo.index.add([resource_path])
        repo.index.commit("First commit")
    except ConnectionError:
        return redirect('%s/dataset/%s/resource/%s/git/branches?error=True' %
                        (SITE_URL, resource['package_id'], resource['id']))


def delete_repo(resource):
    shutil.rmtree(os.path.join(REPO_DIR, resource['id']))


def check_repo(f):
    def wrapper(*args, **kwargs):
        if 'resource_id' in kwargs:
            resource_dict = get.resource_show({'model': ckan.model},
                                              {'id': kwargs['resource_id']})
            if not os.path.exists(os.path.join(REPO_DIR, resource_dict['id'])):
                create_repo(resource_dict)
            elif not os.path.isfile(os.path.join(REPO_DIR, resource_dict['id'],
                                    resource_dict['url'].rsplit('/', 1)[-1])):
                repo = git.Repo.init(os.path.join(REPO_DIR,
                                     resource_dict['id']))
                try:
                    response = requests.get(resource_dict['url'])
                    resource_file_name = response.url.rsplit('/', 1)[-1]
                    resource_path = os.path.join(REPO_DIR, resource_dict['id'],
                                                 resource_file_name)
                    _file = open(resource_path, 'w')
                    _file.write(response.text.encode('utf-8'))
                    _file.close()
                    repo.index.add([resource_path])
                    repo.index.commit("First commit")
                except ConnectionError:
                    return redirect('%s/dataset/%s/resource/%s/git/branches?'
                                    'error=True' %
                                    (SITE_URL, resource_dict['package_id'],
                                     resource_dict['id']))

        return f(*args, **kwargs)
    return wrapper
